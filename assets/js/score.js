const tbody = document.querySelector('tbody');

// 发送查询请求 获取数据 渲染页面 封装函数
function getScoreList(params) {
    axios.get("/score/list",).then(result => {  //无需携带token
        if (result.data.code === 0) { //判断获取成功
            console.log(result);
            const obj = result.data.data
            let html = '';
            //forin循环遍历对象
            for (const key in obj) {
                //循环遍历成绩 //成绩为对象里的数组，因此采用数组方法reduce   //里面放入一个input输入框
                const grades = obj[key].score.reduce((h, v, i) => h + `   
                       <td class="score">${v} <input type="text"  data-id=${key}  data-batch=${i + 1} ></td> 
                `, '')

                //组装html
                html += `
             <tr>
                <th scope="row">${key}</th>
                <td>${obj[key].name}</td>   
                    ${grades}
             </tr>`
            }

            tbody.innerHTML = html

            //获取此时tbody里面的的输入框 当出现失去焦点事件直接返回数值给后端
            const inputAll = document.querySelectorAll('tbody input');

            //遍历inputAll数组
            inputAll.forEach(input => {
                input.addEventListener('blur', function (e) {

                    const f = (!isNaN(input.value) && input.value <= 100 && input.value > 0) //ture

                    if (!f) {
                        toastr.error("请输入1-100的数字");
                        input.focus();
                        return
                    }

                    //axios更改事件 此时传入data参数 发送给后端
                    data = {
                        "stu_id": input.dataset.id,
                        "batch": input.dataset.batch,
                        "score": input.value,
                    }


                    axios.post('score/entry', data).then(result => {
                        //判断成功了即可提示编辑成功并刷新数据 
                        if (result.data.code === 0) {
                            toastr.success(result.data.message);
                            getScoreList();
                        }
                    })
                })

            })

        }
    })

}

//双击绑定事件 委托事件  双击触发input框获得焦点
//判断是否为成绩框 是成绩框才能触发焦点 增加focus() 方法
//往 每个 元素里添加自定义属性  


//给分数单元格 绑定双击事件
tbody.addEventListener("dblclick", function (e) {
    const dom = e.target
    //确认是点击的分数单元格
    if (dom.nodeName = 'TD' && dom.classList.contains('score')) {
        //获取input输入框
        const input = dom.querySelector('input');
        input.style.display = 'block';

        //获取此input 的值
        input.value = dom.innerText

        //获取此input 的焦点
        input.focus();

    }

})

getScoreList();

