//发出查询请求 并附带上token 这个token 是存储 在get  请求头上的
axios.get('/student/list',).then(result => {
    console.log(result);
})


const logout = document.querySelector('.logout a');  //退出元素

logout.addEventListener("click", function () {  //点击链接退出
    if (confirm("您确定要退出吗")) {
        localStorage.removeItem("token82")   //删除本地存储token 
        location.href = "./login.html"     //js相对路径跳转到登录页面
    }
})

//点击图标概览显示数据 一级菜单  
//两个功能 1 小图标旋转90度 2 显示二级菜单
const oneMenuList = document.querySelectorAll('.nav>li>a ');   //获取两个一级菜单
const twoMenuList = document.querySelectorAll('.nav ul a');   //获取两个一级菜单
const initBtn = document.querySelector('.init');

//循环遍历判断点击
oneMenuList.forEach(one => {
    one.addEventListener('click', function () {   //点击触发
        //切换类名
        one.querySelector(".toggle").classList.toggle('toggleone');
        //显示二级菜单
        one.nextElementSibling.classList.toggle("show");
    })
})

//二级菜单功能
twoMenuList.forEach(two => {
    two.addEventListener('click', function () {   //点击触发
        //二级菜单去除active  重新添加active
        document.querySelector('.nav ul a.active').classList.remove('active');
        two.classList.add('active');
    })
})

initBtn.addEventListener('click', function () {
    axios.get('/init/data').then(result => {
    })
})