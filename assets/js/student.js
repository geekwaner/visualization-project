//渲染学生页面
const tbody = document.querySelector('tbody');

//获取数据 学员成绩
function getStudentList(params) {
    //查询所有学员
    axios.get('/student/list').then(result => {
        if (result.data.code === 0) {
            const html = result.data.data.reduce((h, v) => h + `
        <tr>
        <th scope="row">${v.id}</th>
        <td>${v.name}</td>
        <td>${v.age}</td>
        <td>${v.sex}</td>
        <td>${v.group}</td>
        <td>${v.phone}</td>
        <td>${v.salary}</td>
        <td>${v.truesalary}</td>
        <td>${v.province + v.city + v.county}</td>
        <td>
          <button type="button" class="btn btn-primary btn-sm  update-btn" data-id='${v.id}' >修改</button>
          <button type="button" class="btn btn-danger btn-sm   delete-btn" data-id='${v.id}'>删除</button>
        </td>
        </tr>
            `, '')

            tbody.innerHTML = html
        }


    })
}

//封装axios  url params dom text
function renderSelect(url, params, dom, text, value = '') {
    axios.get(url, { params }).then(result => {
        //给数据做循环
        dom.innerHTML = result.data.reduce((h, v) => h + `<option value="${v}">${v}</option>`, `<option value="">--${text}--</option>`);
        //给数据
        dom.value = value
    })
}

//封装新增 学员省份
function initSelect(selector) {

    const provinceSelect = document.querySelector(`${selector} [name=province]`);   //省份
    const citySelect = document.querySelector(`${selector} [name=city]`);          //市
    const countySelect = document.querySelector(`${selector} [name=county]`);      //县

    //添加学员 省份
    //获取省份文档
    renderSelect('/geo/province', {}, provinceSelect, "省")


    //获取市  change  点击事件
    provinceSelect.addEventListener('change', function () {
        renderSelect('/geo/city', { pname: provinceSelect.value }, citySelect, '市')
    })

    //获取县 change 点击事件
    citySelect.addEventListener('change', function () {
        renderSelect('/geo/county', { pname: provinceSelect.value, cname: citySelect.value }, countySelect, '县')
    })
}


//获取数据 调用函数
getStudentList()

//新增表单 下拉列表  //省份
initSelect('#addModal')
// 编辑表单 下拉列表  //省份
initSelect('#updateModal')

//获取表单    
const addForm = document.querySelector('#addModal form');
// 表单submit提交事件
addForm.addEventListener('submit', function (e) {
    e.preventDefault();

    const data = $(addForm).serialize()
    console.log(data);
    axios.post('/student/add', data).then(result => {
        if (result.data.code === 0) {
            //提示添加成功
            toastr.success(result.data.message)
            //表单重置
            addForm.reset();
            //隐藏模态框
            $('#addModal').modal('hide');
            //重新获取
            getStudentList()
        }
    })
})


const updata = document.querySelector('.update-btn');

//触发模态框事件
tbody.addEventListener('click', function (e) {
    if (e.target.classList.contains('update-btn')) {
        //触发模态框
        $('#updateModal').modal('show');
        //把数据放入模态框  获取id值
        const id = e.target.dataset.id
        axios.get('/student/one', { params: { id } }).then(result => {
            if (result.data.code === 0) {
                //查询成功
                //forin 遍历对象
                const obj = result.data.data
                for (const key in obj) {
                    const dom = document.querySelector(`#updateModal [name=${key}]`);

                    //如果为key=sex
                    if (key === 'sex') {
                        document.querySelector(`#updateModal [name=sex][value=${obj.sex}]`).checked = true;
                        continue
                    }

                    //获取dom元素 
                    if (dom) {
                        dom.value = obj[key]
                    }

                    //调用axios函数 显示下拉菜单以及选中值  市
                    renderSelect('/geo/city', { pname: obj.province }, document.querySelector('#updateModal [name=city]'), '市', obj.city);
                    renderSelect('/geo/county', { pname: obj.province, cname: obj.city }, document.querySelector('#updateModal [name=county]'), '县', obj.county);

                }
            }
        })

    } else if (e.target.classList.contains('delete-btn')) {
        //先询问确定是否删除
        if (confirm('您是否确认删除')) {
            //获取其id
            const id = e.target.dataset.id
            axios.delete('/student/delete', { params: { id } }).then(result => {
                if (result.data.code === 0) {
                    toastr.success(result.data.message);
                    getStudentList();
                }
            })
        }
    }
})

//修改表单事件   获取修改表单
const updateForm = document.querySelector('#updateModal form');
// 表单submit提交事件
updateForm.addEventListener('submit', function (e) {
    e.preventDefault();

    const data = $(updateForm).serialize()
    axios.put('/student/update', data).then(result => {
        if (result.data.code === 0) {
            //提示添加成功
            toastr.success(result.data.message)
            //表单重置
            updateForm.reset();
            //隐藏模态框
            $('#updateModal').modal('hide');
            //重新获取
            getStudentList()
        }
    })
})

