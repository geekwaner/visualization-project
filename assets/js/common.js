//接口的根路径
axios.defaults.baseURL = 'http://www.itcbc.com:8000';



// 添加请求拦截器
axios.interceptors.request.use(function (config) {
    // 在发送请求之前做些什么
    console.log(config);
    // 在请求拦截器中 判断是否有携带token  如果有则在拦截器中添加请求头属性
    //好处 ：不用每次发送请求都写token参数
    if (localStorage.getItem('token82')) {
        config.headers.Authorization = localStorage.getItem('token82')
    }

    NProgress.start()
    return config;
}, function (error) {
    // 对请求错误做些什么
    return Promise.reject(error);
});



// 添加响应拦截器
axios.interceptors.response.use(function (response) {
    // 2xx 范围内的状态码都会触发该函数。
    if (response.data.code && response.data.code !== 0) {    //增加有状态码条件 完成两个条件才执行if
        toastr.error(response.data.message);
    }

    NProgress.done()  //进度条关闭
    return response;
}, function (error) {
    console.dir(error)  //可以从里面拿取状态码
    // 判断一下 http 响应状态码 是不是 401  401为身份认证失败 没有token 跳转登录界面
    if (error.response.status === 401) {
        window.top.location.href = "./login.html"    //跳转回登录页面  window.top 最顶端适用与父子页面
    }

    toastr.error(error.response.data.message);  //错误400，提示错误信息
    NProgress.done()  //进度条加载关闭
    // 超出 2xx 范围的状态码都会触发该函数。
    return Promise.reject(error);
});
