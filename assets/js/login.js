//登录注册功能
// 调用一次 测试是否标头对了

// axios({
//     url: '/student/list',
//     method: 'get',
// }).then(result => {
//     console.log(result);
// })

//获取登录和注册盒子
const registerBox = document.querySelector('.register');  //注册盒子
const loginBox = document.querySelector('.login');     //登录盒子
const registerA = document.querySelector('.register-a');  //注册点击切换
const loginA = document.querySelector('.login-a');      //登录点击切换


//点击事件
//点击注册 切换为显示登录 隐藏注册
loginA.addEventListener('click', function () {
    loginBox.style.display = 'none';     //登录盒子隐藏
    registerBox.style.display = 'block';  //注册盒子显示
})

registerA.addEventListener('click', function () {
    registerBox.style.display = 'none';  //注册盒子隐藏
    loginBox.style.display = 'block';  //登录盒子显示
})


//给注册表单绑定点击事件
registerBox.querySelector('form').addEventListener('submit', function (event) {
    //阻止默认行为  
    event.preventDefault()

    //jq方法快速获取表单
    const data = $(this).serialize()
    console.log(data);

    //axios新增数据
    axios.post('/api/register', data).then(result => {
        //判断是否成功新增数据  我们只判断成功后完成的步骤 ，失败的提示交给拦截器
        if (result.data.code === 0) {
            toastr.success(result.data.message);
            //触发点击  显示登录界面
            registerA.click();
        }
    })

})

//给登录表单绑定点击事件
loginBox.querySelector('form').addEventListener('submit', function (e) {
    //阻止默认事件
    e.preventDefault();

    //jq方法快速获取表单
    const data = $(this).serialize()

    //axios 登录
    axios.post('/api/login', data).then(result => {
        // console.log(result);
        //判断是否返回
        if (result.data.code === 0) {
            //把token 储存到本地
            localStorage.setItem('token82', result.data.token)
            //提示登录成功
            toastr.success(result.data.message);
            //点击跳转到index页面

            setTimeout(() => {
                location.href = './index.html'
            }, 1500);
        }
    })


})
