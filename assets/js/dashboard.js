// 班级概览
const toggleBatchBtn = document.querySelector('#toggleBatchBtn');
const batch = document.querySelector('#batch');

function getStudentOver(params) {
    //发送请求
    axios.get('/student/overview').then(result => {
        if (result.data.code === 0) {
            console.log(result);
            const { avgAge, avgSalary, proportion, total } = result.data.data
            document.querySelector('.avgAge').innerText = avgAge;
            document.querySelector('.avgSalary').innerText = avgSalary;
            document.querySelector('.proportion').innerText = proportion;
            document.querySelector('.total').innerText = total;
        }
    })
}

getStudentOver()  //调用数据


getStudentList()

//获取学生信息
function getStudentList() {
    axios.get('/student/list').then(result => {
        const list = result.data.data
        salaryLine(list)   //薪资折线
        homeTown(list)   //饼图籍贯
        getScore()
        getMap()
    })
}

//先获取各个数据


//薪资折线图
function salaryLine(list) {

    //map方法输出新的数组  名字 期望薪资 实际薪资
    const salarys = list.map(v => v.salary)
    const truesalarys = list.map(v => v.truesalary)
    const names = list.map(v => v.name)

    //建立一个div盒子
    const myChart = echarts.init(document.querySelector('.line'));

    const option = {
        title: {
            text: '薪资 Salary'
        },
        tooltip: {
            trigger: 'axis'
        },
        legend: {},
        toolbox: {
            show: false,
            feature: {
                dataZoom: {
                    yAxisIndex: 'none'
                },
                dataView: { readOnly: false },
                magicType: { type: ['line', 'bar'] },
                restore: {},
                saveAsImage: {}
            }
        },
        xAxis: {
            type: 'category',
            boundaryGap: false,
            data: names,
        },
        yAxis: {
            type: 'value',

        },
        dataZoom: [    //控制数据缩放
            {
                type: 'inside',
                start: 0,
                end: 10
            },
            {
                start: 0,
                end: 10
            }
        ],
        series: [
            {
                name: '期望薪资',
                type: 'line',    //曲线图
                smooth: true, // 线条平滑
                symbol: 'circle', // 实心
                data: salarys,

            },
            {
                name: '实际薪资',
                type: 'line',       //折线图
                smooth: true, // 线条平滑
                symbol: 'circle', // 实心
                data: truesalarys,

                markLine: {
                    data: [
                        [
                            {
                                symbol: 'none',
                                x: '90%',
                                yAxis: 'max'
                            },
                            {
                                symbol: 'circle',
                                label: {
                                    position: 'start',
                                    formatter: 'Max'
                                },

                            }
                        ]
                    ]
                }
            }
        ]
    };
    // 使用刚指定的配置项和数据显示图表。
    myChart.setOption(option);


}

//家乡饼图
function homeTown(list) {
    //给list数据做循环
    //判断当前遍历的元素是否存在于新数组中
    //不在即存入新数组
    //在的话则index++

    let newArr = [];
    list.forEach(v => {
        const index = newArr.findIndex(vv => vv.name === v.province)
        //判断如果找不到index返回-1
        if (index === -1) {
            newArr.push({ value: 1, name: v.province })
        } else {
            newArr[index].value++;
        }

    })


    //建立一个div盒子
    const chartDom = document.querySelector('.pie');
    const myChart = echarts.init(chartDom);
    let option;

    option = {
        series: [
            {
                name: 'Nightingale Chart',
                type: 'pie',
                radius: [50, 150],
                center: ['50%', '50%'],
                roseType: 'area',
                itemStyle: {
                    borderRadius: 8
                },
                data: newArr
            }
        ]
    };

    option && myChart.setOption(option);

}

//成绩柱状图 Score
function getScore(batch = 1) {
    //封装第几次成绩函数
    axios('/score/batch', { params: { batch } }).then(result => {
        console.log(result);
        const { avgScore, group, gt60, gt80, lt60 } = result.data.data;


        const myChart = echarts.init(document.querySelector('.barChart'));

        const option = {
            grid: {
                top: 30,
                bottom: 20
            },

            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'cross',
                    crossStyle: {
                        color: '#999'
                    }
                }
            },

            legend: {
                data: ['平均分', '低于60分人数', '60-80分人数', '高于80分人数']
            },
            xAxis: [
                {
                    type: 'category',
                    data: group,
                    axisPointer: {
                        type: 'shadow'
                    }
                }
            ],
            yAxis: [
                {
                    type: 'value',
                    name: `第${batch}组`,
                    min: 0,
                    max: 100,
                    interval: 10,
                    axisLabel: {
                        formatter: '{value}分'
                    }
                },
                {
                    type: 'value',
                    min: 0,
                    max: 10,
                    interval: 1,
                    axisLabel: {
                        formatter: '{value}人'
                    }
                }
            ],
            series: [
                {
                    name: '平均分',
                    type: 'bar',
                    tooltip: {
                        valueFormatter: function (value) {
                            return value + ' ml';
                        }
                    },
                    data: avgScore
                },
                {
                    name: '低于60分人数',
                    type: 'bar',
                    yAxisIndex: 1,

                    tooltip: {
                        valueFormatter: function (value) {
                            return value + ' ml';
                        }
                    },
                    data: lt60
                },
                {
                    name: '60-80分人数',
                    type: 'bar',
                    yAxisIndex: 1,
                    tooltip: {
                        valueFormatter: function (value) {
                            return value + ' ml';
                        }
                    },
                    data: gt60
                },
                {
                    name: '高于80分人数',
                    type: 'bar',
                    yAxisIndex: 1,
                    tooltip: {
                        valueFormatter: function (value) {
                            return value + ' ml';
                        }
                    },
                    data: gt80
                },

            ]
        };

        myChart.setOption(option);
    })
}


//地图map图
function getMap() {
    // console.log(mapData);

    const myChart = echarts.init(document.querySelector('.map'));

    const points = [
        { value: [118.8062, 31.9208], itemStyle: { color: '#4ab2e5' } },
        { value: [127.9688, 45.368], itemStyle: { color: '#4fb6d2' } },
        { value: [110.3467, 41.4899], itemStyle: { color: '#52b9c7' } },
        { value: [125.8154, 44.2584], itemStyle: { color: '#5abead' } },
        { value: [116.4551, 40.2539], itemStyle: { color: '#f34e2b' } },
        { value: [123.1238, 42.1216], itemStyle: { color: '#f56321' } },
        { value: [114.4995, 38.1006], itemStyle: { color: '#f56f1c' } },
        { value: [117.4219, 39.4189], itemStyle: { color: '#f58414' } },
        { value: [112.3352, 37.9413], itemStyle: { color: '#f58f0e' } },
        { value: [109.1162, 34.2004], itemStyle: { color: '#f5a305' } },
        { value: [103.5901, 36.3043], itemStyle: { color: '#e7ab0b' } },
        { value: [106.3586, 38.1775], itemStyle: { color: '#dfae10' } },
        { value: [101.4038, 36.8207], itemStyle: { color: '#d5b314' } },
        { value: [103.9526, 30.7617], itemStyle: { color: '#c1bb1f' } },
        { value: [108.384366, 30.439702], itemStyle: { color: '#b9be23' } },
        { value: [113.0823, 28.2568], itemStyle: { color: '#a6c62c' } },
        { value: [102.9199, 25.46639], itemStyle: { color: '#96cc34' } },
        { value: [119.4543, 25.9222] },
    ];
    echarts.registerMap('china', mapData); // geoJson 请求回来的数据 变量
    const option = {
        backgroundColor: '#013954',
        geo: {
            map: 'china',
            aspectScale: 0.75, //长宽比
            zoom: 1.1,
            roam: false,
            itemStyle: {
                normal: {
                    areaColor: {
                        type: 'radial',
                        x: 0.5,
                        y: 0.5,
                        r: 0.8,
                        colorStops: [
                            {
                                offset: 0,
                                color: '#09132c', // 0% 处的颜色
                            },
                            {
                                offset: 1,
                                color: '#274d68', // 100% 处的颜色
                            },
                        ],
                        globalCoord: true, // 缺省为 false
                    },
                    shadowColor: 'rgb(58,115,192)',
                    shadowOffsetX: 10,
                    shadowOffsetY: 11,
                },
                emphasis: {
                    areaColor: '#2AB8FF',
                    borderWidth: 0,
                    color: 'green',
                    label: {
                        show: false,
                    },
                },
            },
            regions: [
                {
                    name: '南海诸岛',
                    itemStyle: {
                        areaColor: 'rgba(0, 10, 52, 1)',

                        borderColor: 'rgba(0, 10, 52, 1)',
                        normal: {
                            opacity: 0,
                            label: {
                                show: false,
                                color: '#009cc9',
                            },
                        },
                    },
                },
            ],
        },
        series: [
            {
                type: 'map',
                roam: false,
                label: {
                    normal: {
                        show: true,
                        textStyle: {
                            color: '#1DE9B6',
                        },
                    },
                    emphasis: {
                        textStyle: {
                            color: 'rgb(183,185,14)',
                        },
                    },
                },

                itemStyle: {
                    normal: {
                        borderColor: 'rgb(147, 235, 248)',
                        borderWidth: 1,
                        areaColor: {
                            type: 'radial',
                            x: 0.5,
                            y: 0.5,
                            r: 0.8,
                            colorStops: [
                                {
                                    offset: 0,
                                    color: '#09132c', // 0% 处的颜色
                                },
                                {
                                    offset: 1,
                                    color: '#274d68', // 100% 处的颜色
                                },
                            ],
                            globalCoord: true, // 缺省为 false
                        },
                    },
                    emphasis: {
                        areaColor: 'rgb(46,229,206)',
                        //    shadowColor: 'rgb(12,25,50)',
                        borderWidth: 0.1,
                    },
                },
                zoom: 1.1,
                //     roam: false,
                map: 'china', //使用
                // data: this.difficultData //热力图数据   不同区域 不同的底色
            },
            {
                type: 'effectScatter',
                coordinateSystem: 'geo',
                showEffectOn: 'render',
                zlevel: 1,
                rippleEffect: {
                    period: 15,
                    scale: 4,
                    brushType: 'fill',
                },
                hoverAnimation: true,
                label: {
                    normal: {
                        formatter: '{b}',
                        position: 'right',
                        offset: [15, 0],
                        color: '#1DE9B6',
                        show: true,
                    },
                },
                itemStyle: {
                    normal: {
                        color: '#1DE9B6' /* function (value){ //随机颜色
 return "#"+("00000"+((Math.random()*16777215+0.5)>>0).toString(16)).slice(-6);
 }*/,
                        shadowBlur: 10,
                        shadowColor: '#333',
                    },
                },
                symbolSize: 12,
                data: points,
            }, //地图线的动画效果
            {
                type: 'lines',
                zlevel: 2,
                effect: {
                    show: true,
                    period: 4, //箭头指向速度，值越小速度越快
                    trailLength: 0.4, //特效尾迹长度[0,1]值越大，尾迹越长重
                    symbol: 'arrow', //箭头图标
                    symbolSize: 7, //图标大小
                },
                lineStyle: {
                    normal: {
                        color: '#1DE9B6',
                /* function (value){ //随机颜色
                        
                        ['#f21347','#f3243e','#f33736','#f34131','#f34e2b',
                        '#f56321','#f56f1c','#f58414','#f58f0e','#f5a305',
                        '#e7ab0b','#dfae10','#d5b314','#c1bb1f','#b9be23',
                        '#a6c62c','#96cc34','#89d23b','#7ed741','#77d64c',
                        '#71d162','#6bcc75','#65c78b','#5fc2a0','#5abead',
                        '#52b9c7','#4fb6d2','#4ab2e5']
 return "#"+("00000"+((Math.random()*16777215+0.5)>>0).toString(16)).slice(-6);
 }*/ width: 1, //线条宽度
                        opacity: 0.1, //尾迹线条透明度
                        curveness: 0.3, //尾迹线条曲直度
                    },
                },
                data: [
                    {
                        coords: [
                            [118.8062, 31.9208],
                            [119.4543, 25.9222],
                        ],
                        lineStyle: { color: '#4ab2e5' },
                    },
                    {
                        coords: [
                            [119.4543, 25.9222],
                            [127.9688, 45.368],
                        ],
                        lineStyle: { color: '#4fb6d2' },
                    },
                    {
                        coords: [
                            [110.3467, 41.4899],
                            [119.4543, 25.9222],
                        ],
                        lineStyle: { color: '#52b9c7' },
                    },
                    {
                        coords: [
                            [125.8154, 44.2584],
                            [119.4543, 25.9222],
                        ],
                        lineStyle: { color: '#5abead' },
                    },
                    {
                        coords: [
                            [116.4551, 40.2539],
                            [119.4543, 25.9222],
                        ],
                        lineStyle: { color: '#f34e2b' },
                    },
                    {
                        coords: [
                            [123.1238, 42.1216],
                            [119.4543, 25.9222],
                        ],
                        lineStyle: { color: '#f56321' },
                    },
                    {
                        coords: [
                            [114.4995, 38.1006],
                            [119.4543, 25.9222],
                        ],
                        lineStyle: { color: '#f56f1c' },
                    },
                    {
                        coords: [
                            [117.4219, 39.4189],
                            [119.4543, 25.9222],
                        ],
                        lineStyle: { color: '#f58414' },
                    },
                    {
                        coords: [
                            [112.3352, 37.9413],
                            [119.4543, 25.9222],
                        ],
                        lineStyle: { color: '#f58f0e' },
                    },
                    {
                        coords: [
                            [109.1162, 34.2004],
                            [119.4543, 25.9222],
                        ],
                        lineStyle: { color: '#f5a305' },
                    },
                    {
                        coords: [
                            [103.5901, 36.3043],
                            [119.4543, 25.9222],
                        ],
                        lineStyle: { color: '#e7ab0b' },
                    },
                    {
                        coords: [
                            [106.3586, 38.1775],
                            [119.4543, 25.9222],
                        ],
                        lineStyle: { color: '#dfae10' },
                    },
                    {
                        coords: [
                            [101.4038, 36.8207],
                            [119.4543, 25.9222],
                        ],
                        lineStyle: { color: '#d5b314' },
                    },
                    {
                        coords: [
                            [103.9526, 30.7617],
                            [119.4543, 25.9222],
                        ],
                        lineStyle: { color: '#c1bb1f' },
                    },
                    {
                        coords: [
                            [108.384366, 30.439702],
                            [119.4543, 25.9222],
                        ],
                        lineStyle: { color: '#b9be23' },
                    },
                    {
                        coords: [
                            [113.0823, 28.2568],
                            [119.4543, 25.9222],
                        ],
                        lineStyle: { color: '#a6c62c' },
                    },
                    {
                        coords: [
                            [102.9199, 25.46639],
                            [119.4543, 25.9222],
                        ],
                        lineStyle: { color: '#96cc34' },
                    },
                ],
            },
        ],
    };
    myChart.setOption(option, true);
}


//点击事件
toggleBatchBtn.addEventListener('click', function (e) {
    batch.classList.toggle('show');
})

batch.addEventListener('click', function (e) {
    if (e.target.nodeName === 'A') {
        const index = e.target.dataset.index
        getScore(index)
    }
})
